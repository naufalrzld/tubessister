import xmlrpc.client

s = xmlrpc.client.ServerProxy('http://localhost:8000')

with open("TrainsetTugas1ML.csv",'rb') as handle:
    data=xmlrpc.client.Binary(handle.read())

    s.file_upload(data, "DataTrain.csv")
    print('File Train berhasil diupload')

with open("TestsetTugas1ML.csv",'rb') as handle:
    data=xmlrpc.client.Binary(handle.read())

    s.file_upload(data, "DataTest.csv")
    print('File Test berhasil diupload')

print(s.naive_bayes())

with open("HasilNaiveBayes.csv","wb") as handle:
    handle.write(s.file_download().data)