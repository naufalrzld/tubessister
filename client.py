import xmlrpc.client
import os
import csv
from flask import Flask, render_template, request, send_from_directory, send_file
from werkzeug import secure_filename

UPLOAD_FOLDER = 'uploads'
DOWNLOAD_FOLDER = 'downloads'
IP_HOST = "localhost"
PORT_HOST = 5000

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER

s = xmlrpc.client.ServerProxy('http://'+IP_HOST+':8000')

@app.route('/')
def template():
    return render_template('upload.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f1 = request.files['file_1']
        f2 = request.files['file_2']
        filename1 = secure_filename(f1.filename)
        filename2 = secure_filename(f2.filename)
        f1.save(os.path.join(app.config['UPLOAD_FOLDER'], filename1))
        f2.save(os.path.join(app.config['UPLOAD_FOLDER'], filename2))
        return show_data(f1.filename, f2.filename)
    else:
        return 'File belum diupload!'

@app.route('/hasil')
def hasil():
    print(s.naive_bayes())
    fname = 'TebakanTugas1ML.csv'
    data_test = getFileCSV(fname, 'd')
    data = [fname, data_test]
    return render_template('hasil.html', result=data)

@app.route('/download/<path:filename>')
def download(filename):
    uploads = os.path.join(app.config['DOWNLOAD_FOLDER'], filename)
    print(uploads)
    return send_file(uploads, attachment_filename=filename)


def getFileCSV(filename, type_folder='u'):
    data_set = []
    folder = ""
    if type_folder == 'u':
        folder = 'uploads/'
    else:
        folder = 'downloads/'
    with open(folder+filename) as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        next(reader)
        for data in reader:
            data_set.append(data)

    return data_set

def show_data(filename1, filename2):
    data_train = getFileCSV(filename1)
    data_test = getFileCSV(filename2)
    result = [filename1, filename2, data_train, data_test]
    return render_template('show.html', result=result)

if __name__ == '__main__':
    app.run(host=IP_HOST, port=PORT_HOST, debug=True)
