from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

import xmlrpc.client

import csv

class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_path = ('/RPC2',)

with SimpleXMLRPCServer(("localhost", 8000), requestHandler = RequestHandler) as server:
    server.register_introspection_functions()

    def getFileCSV(file_name, file_type):
        dataTrain = []
        json = {}
        with open(file_name) as file_train_set:
            reader = csv.reader(file_train_set, delimiter=',')
            next(reader)
            for row in reader:
                json = {
                    "id": row[0],
                    "age": row[1],
                    "workclass": row[2],
                    "education": row[3],
                    "marital-status": row[4],
                    "occupation": row[5],
                    "relationship": row[6],
                    "hours-per-week": row[7]
                }

                if file_type != 'test':
                    json['income'] = row[8]

                dataTrain.append(json)
        return dataTrain

    def probIncome(x):
        jum = 0
        for data in getFileCSV('uploads/TrainsetTugas1ML.csv', 'train'):
            if data['income'] == x:
                jum += 1
        
        return jum, jum/len(getFileCSV('uploads/TrainsetTugas1ML.csv', 'train'))

    def probPosterior(key1, x, key2, y):
        jum = 0
        for data in getFileCSV('uploads/TrainsetTugas1ML.csv', 'train'):
            if data[key2] == y and data[key1] == x:
                jum += 1

        jumData, prob = probIncome(y)

        return jum, jum/jumData, str(jum)+'/'+str(jumData)

    def findProb(dataTest, income):
        i = 0
        hasil = 1
        for key in dataTest.keys():
            if i != 0:
                jum, prob, ket = probPosterior(key, dataTest[key], 'income', income)
                hasil *= prob
            i += 1
        jum, income = probIncome(income)
        return hasil * income

    def main():
        arrHasil = []
        for data in getFileCSV('uploads/TestsetTugas1ML.csv', 'test'):
            a = findProb(data, '>50K')
            b = findProb(data, '<=50K')

            hasil = ''
        
            if a > b:
                hasil = '>50K'
            else:
                hasil = '<=50K'
            print(hasil)
            data['income'] = hasil
            arrHasil.append(data)
        print("PROSES KLASIFIKASI SELESAI")

        with open('downloads/TebakanTugas1ML.csv', mode='w') as csv_file:
            fieldnames = ['id','age','workclass','education','marital-status','occupation','relationship','hours-per-week','income']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            writer.writeheader()
            for data in arrHasil:
                writer.writerow(data)
        return arrHasil
    server.register_function(main, 'naive_bayes')

    def file_upload(filedata, filename):
        with open(filename,'wb') as handle:
            data1=filedata.data
            handle.write(data1)
            return True
    server.register_function(file_upload, 'file_upload')

    def file_download():
        with open("downloads/TebakanTugas1ML.csv",'rb') as handle:
            return xmlrpc.client.Binary(handle.read())
    server.register_function(file_download, 'file_download')

    server.serve_forever()