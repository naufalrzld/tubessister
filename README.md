# TUBES SISTEM TERDISTRIBUSI BigComputational"Naive Bayes"

Kelompok:

Mochammad Naufal Rizaldi    (1301160334)

Rafli Alwan Nugraha         (1301164028)    

Nanda Safirah Ihzanti       (1301164322)

Muhammad Faisal Nur         (1301164372)

Kelas: IF-40-12

## Requirement

*  Python 3.7

## How to Run

1) Jalankan script server terlebih dahulu dengan command `python3 rpc_server.py` pada terminal
2) Jalankan script client dengan command `python3 rpc_client.py` pada terminal

## Analisis

### Server


![](./gbr/server.jpeg)

Pada gambar diatas server dijalankan


#### Client

![](./gbr/client.png)

Pada gambar diatas client mengupload file `TrainsetTugas1ML.csv` dan `TestsetTugas1ML.csv` ke server

#### Melakukan Naive Bayes

![](./gbr/file.png)

setelah melakukan naive bayes setelah itu mendapatkan file `TebakTugas1ML.csv`

## Fitur Show Data Train & Data Test
PIC: Mochammad Naufal Rizaldi (1301160334)
### Data Train
![](./gbr/show_data_train.png)
### Data Test
![](./gbr/show_data_test.png)
Kedua gambar diatas adalah tampilan data train dan data test yang diambil dari csv.

## Fitur Show Data Hasil Naive bayes
PIC: muhammad Faisal Nur(1301164372)
### Data Hasil Naive Bayes
![](./gbr/hasil.png)

Pada gambar diatas adalah tampilan data hasil peroses Naive bayes.

## Fitur Upload Data 
Nanda Safira I (1301164322)

Fitur ini merupakan fitur dimana client dapat meng-upload file yang akan di lakukan klasifikasi. File yang di upload berupa file dalam bentuk .csv 

## Fitur Download hasil naive bayes
PIC: Rafli Alwan Nugraha(1301164028)
### Fitur Download Naive Bayes
![](./gbr/1.Download.png)
![](./gbr/2.Download.png)

Pada gambar diatas adalah tampilan fitur download yang telah di buat untuk mendownload file .csv dari hasil program yang telah dilakukan